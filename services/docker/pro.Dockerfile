FROM node:10

COPY ./ /srv/service

WORKDIR /srv/service

ENV NODE_ENV "develop"

ENV TERM "xterm-256color"

ENV FORCE_COLOR 1
ENV DEBUG_COLORS "true"
ENV DEBUG "jolorenzom*"

RUN rm -rf ./node_modules

RUN npm install

RUN npm run build

RUN echo $NODE_ENV

EXPOSE 3001

CMD npm run start
