import {
  Controller,
  ForbiddenException,
  Get,
  Param,
  Post,
  Res,
  UseGuards,
} from '@nestjs/common';
import { moduleConfig } from './image.config';
import { UseInterceptors, UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { AuthGuard } from '@nestjs/passport';
import * as fse from 'fs-extra';
import { config } from '../../../config/config';

@Controller(moduleConfig.api.route)
export class ImageController {
  @Get(`:${moduleConfig.api.param}`)
  async serveImage(
    @Param(`${moduleConfig.api.param}`) id: string,
    @Res() res,
  ): Promise<any> {
    res.sendFile(id, { root: moduleConfig.static.route });
  }

  @UseGuards(AuthGuard()) // Protected with auth
  @Post()
  @UseInterceptors(
    FileInterceptor(moduleConfig.api.param, {
      storage: diskStorage({
        destination:
          __dirname + '../../../../../../' + moduleConfig.static.route,
        filename: (req, image, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          return cb(null, `${randomName}${extname(image.originalname)}`);
        },
      }),
    }),
  )
  uploadImage(@UploadedFile() image) {
    if (!moduleConfig.acceptedMimeTypes.includes(image.mimetype)) {
      fse.remove(image.path);
      throw new ForbiddenException('invalid-mimetype');
    }
    const root =
      process.env.NODE_ENV === 'production'
        ? config.sites.static.url.production
        : config.sites.static.url.localhost;
    return {
      filename: image.filename,
      path: root + moduleConfig.api.route + '/' + image.filename,
      mimetype: image.mimetype,
    };
  }
}
