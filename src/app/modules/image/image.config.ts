export const moduleConfig = {
  api: {
    route: 'images',
    param: 'image',
  },
  acceptedMimeTypes: [
    'image/jpeg',
    'image/pjpeg',
    'image/png',
    'image/bmp',
    'image/x-windows-bmp',
    'image/gif',
  ],
  static: {
    route: 'artifacts/public/images/',
  },
};
