import { Module } from '@nestjs/common';
import { ImageController } from './image.controller';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [PassportModule.register({ defaultStrategy: 'jwt' })],
  providers: [],
  controllers: [ImageController],
  exports: [],
})
export class ImageModule {}
