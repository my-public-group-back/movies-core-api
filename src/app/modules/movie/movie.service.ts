import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Like, Repository } from 'typeorm';
import { Movie } from './movie.entity';
import { MysqlAbstractService } from '../../../shared/services/mysql.abstract.service';

@Injectable()
export class MovieService extends MysqlAbstractService {
  constructor(
    @InjectRepository(Movie)
    private readonly movieRepository: Repository<Movie>,
  ) {
    super(movieRepository);
  }

  async prepareQuery(params) {
    let query: any = {};

    if (params.page) {
      query.take = 10;
      query.skip = (params.page - 1) * query.take;
    }
    if (params.title) {
      query = Object.assign({}, query, { title: Like(`%${params.title}%`) });
    }
    if (params.genre) {
      query = Object.assign({}, query, { genre: In(params.genre.split(',')) });
    }
    return query;
  }
}
