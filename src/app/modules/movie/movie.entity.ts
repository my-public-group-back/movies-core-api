import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { moduleConfig } from './movie.config';

export type MovieGenre = 'terror' | 'drama' | 'comedy' | 'action';

@Entity(moduleConfig.api.route)
export class Movie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', unique: false, nullable: false })
  title: string;

  @Column({ type: 'varchar', unique: false, nullable: false })
  description: string;

  @Column({
    type: 'enum',
    enum: ['terror', 'drama', 'comedy', 'action'],
    nullable: false,
  })
  genre: MovieGenre;

  @Column({ type: 'varchar', unique: false, nullable: false })
  picture: string;
}
