import { IsEnum, IsNotEmpty, IsString } from 'class-validator';

export class MovieDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  description: string;

  @IsEnum({
    terror: 'terror',
    drama: 'drama',
    comedy: 'comedy',
    action: 'action',
  })
  @IsNotEmpty()
  genre?: string;

  @IsString()
  @IsNotEmpty()
  picture: string;
}
