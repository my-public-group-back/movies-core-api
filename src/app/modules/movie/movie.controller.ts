import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { MovieService } from './movie.service';
import { Movie } from './movie.entity';
import { moduleConfig } from './movie.config';
import { MovieDto } from './movie.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller(moduleConfig.api.route)
export class MovieController {
  constructor(private readonly movieService: MovieService) {}

  @UseGuards(AuthGuard()) // Protected with auth
  @Post()
  async create(@Body() createMovieBody: MovieDto): Promise<Movie> {
    return await this.movieService.create(createMovieBody);
  }

  @Get()
  async findAll(@Query() params): Promise<Movie[]> {
    return await this.movieService.findAll(params);
  }

  @Get(`:${moduleConfig.api.param}`)
  async findOne(
    @Param(`${moduleConfig.api.param}`) id: number,
  ): Promise<Movie> {
    return await this.movieService.checkEntity(id);
  }

  @UseGuards(AuthGuard()) // Protected with auth
  @Put(`:${moduleConfig.api.param}`)
  async update(
    @Body() updateMovieBody: MovieDto,
    @Param(`${moduleConfig.api.param}`) id: number,
  ): Promise<Movie> {
    await this.movieService.checkEntity(id);
    return await this.movieService.update(id, updateMovieBody);
  }

  @UseGuards(AuthGuard()) // Protected with auth
  @Delete(`:${moduleConfig.api.param}`)
  async delete(
    @Param(`${moduleConfig.api.param}`) id: number,
  ): Promise<boolean> {
    await this.movieService.checkEntity(id);
    return await this.movieService.delete(id);
  }
}
