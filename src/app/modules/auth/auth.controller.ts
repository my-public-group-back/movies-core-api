import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { moduleConfig } from './auth.config';
import { AuthDto } from './auth.dto';

@Controller(moduleConfig.api.route)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  async login(@Body() bodyAuth: AuthDto): Promise<any> {
    return await this.authService.login(bodyAuth);
  }
}
