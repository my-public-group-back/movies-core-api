import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { moduleConfig } from './auth.config';
import * as bcrypt from 'bcryptjs';
import { User } from '../user/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async checkCredentials(username, password, isBcrypt = false): Promise<User> {
    const user = await this.userService.findOne({ username });
    if (!user) {
      throw new UnauthorizedException('incorrect-credentials 1'); // incorrect user
    }
    const comparePasswords =
      isBcrypt === true
        ? bcrypt.compareSync(password, user.password)
        : password === user.password;
    if (!comparePasswords) {
      throw new UnauthorizedException('incorrect-credentials 2'); // incorrect password
    }
    return user;
  }

  async login(credentials): Promise<any> {
    const user = await this.checkCredentials(
      credentials.username,
      credentials.password,
      true,
    );
    const accessToken = this.jwtService.sign(
      { user },
      { expiresIn: moduleConfig.expiresIn },
    );
    return {
      expiresIn: moduleConfig.expiresIn,
      // eslint-disable-next-line @typescript-eslint/camelcase
      access_token: accessToken,
      user,
    };
  }
}
