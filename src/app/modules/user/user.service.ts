import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { MysqlAbstractService } from '../../../shared/services/mysql.abstract.service';

@Injectable()
export class UserService extends MysqlAbstractService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {
    super(userRepository);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  protected async prepareQuery(params): Promise<any> {
    return {};
  }

  async checkUsername(username) {
    const user = await this.findOne({
      username,
    });
    if (user) {
      throw new BadRequestException('username-already-exists');
    }
  }
}
