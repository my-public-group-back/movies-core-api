import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';
import { moduleConfig } from './user.config';
import * as bcrypt from 'bcryptjs';
import { config } from '../../../config/config';
import { UserDto } from './user.dto';

@Controller(moduleConfig.api.route)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async create(@Body() createUserBody: UserDto): Promise<User> {
    await this.userService.checkUsername(createUserBody.username);
    createUserBody.password = bcrypt.hashSync(
      createUserBody.password,
      config.saltRounds,
    );
    return await this.userService.create(createUserBody);
  }
}
