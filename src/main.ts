import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { config } from './config/config';
import { AppModule } from './app/app.module';
import * as cors from 'cors';
import * as debug from 'debug';
import * as helmet from 'helmet';
import * as morgan from 'morgan';

// Set debug
const log: debug.IDebugger = debug('jolorenzom:server');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Log every request to the console
  app.use(morgan(config.api.morgan));

  // Enable CORS middleware
  app.use(cors(config.api.cors));

  // Set Helmet
  app.use(helmet(config.api.helmet));

  // Pipe to responses DTO erros
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(process.env.PORT || config.sites.api.port);

  log(
    "%s's magic happens at %s://%s:%s \nPID: %s.\nEnvironment: %s.\nRoot: %s",
    config.project.name,
    config.sites.api.protocol,
    config.sites.api.url,
    config.sites.api.port,
    process.pid || 'not forked',
    config.environment,
    config.PWD,
  );
}
bootstrap();
