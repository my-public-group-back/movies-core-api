import { NestApplicationOptions } from '@nestjs/common';

export const config = {
  api: {
    config: {
      logger: ['log', 'error', 'warn', 'debug', 'verbose'],
    } as NestApplicationOptions,
    morgan: 'dev',
    helmet: {
      // https://helmetjs.github.io/docs/
      contentSecurityPolicy: false,
      dnsPrefetchControl: true,
      expectCt: false,
      frameguard: {
        action: 'deny',
      },
      hidePoweredBy: {
        setTo: 'jolorenzom',
      },
      hsts: true,
      ieNoOpen: true,
      noCache: false,
      noSniff: true,
      permittedCrossDomainPolicies: true,
      referrerPolicy: true,
      xssFilter: true,
    },
    cors: {
      origin: true,
      methods: ['GET', 'PATCH', 'PUT', 'POST', 'DELETE'],
      allowedHeaders: [
        'Content-Type',
        'Authorization',
        'Content-Length',
        'Content-Language',
      ],
      exposedHeaders: [
        'x-provider',
        'limit',
        'page',
        'count',
        'X-Response-Time',
      ],
    },
  },
};
