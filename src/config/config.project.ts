export const config = {
  project: {
    name: 'Movies API Core',
    description: 'Movies API Core',
    poweredBy: 'jolorenzom',
    email: 'jolorenzom@gmail.com',
  },

  sites: {
    api: {
      port: 3001,
      url: 'localhost',
      protocol: 'http',
    },
    static: {
      url: {
        production: 'https://movies-core-api.herokuapp.com/',
        localhost: 'http://localhost:3001/',
      },
    },
  },

  databases: {
    mysqlProduction: {
      type: 'mysql',
      host: 'eu-cdbr-west-02.cleardb.net',
      port: 3306,
      username: 'b960dd658c8f6b',
      password: '37fcef6d',
      database: 'heroku_a62fad440e0d73a',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: true,
    },
    mysqlLocalhost: {
      type: 'mysql',
      host: 'localhost',
      port: 33306,
      username: 'root',
      password: 'root',
      database: 'movies',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: true,
    },
  },
  saltRounds: 10,
};
