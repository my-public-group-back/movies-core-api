import { merge } from 'lodash';
import { config as configBase } from './config.base';
import { config as configProject } from './config.project';

// load environment configuration
let config: any = {
  envKey: process.env.ENV_KEY || 'jolorenzom',
  environment: process.env.NODE_ENV || 'development',
  PWD: process.env.PWD,
};

config = merge(configBase, configProject, config);

export { config };
