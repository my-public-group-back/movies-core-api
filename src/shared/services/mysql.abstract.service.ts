import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export abstract class MysqlAbstractService {
  protected abstract async prepareQuery(params): Promise<any>;

  protected constructor(private readonly repository: Repository<any>) {}

  async create(createDto): Promise<any> {
    return await this.repository.save(createDto);
  }

  async findAll(params): Promise<any[]> {
    const query = await this.prepareQuery(params);
    return await this.repository.find(query);
  }

  async findOne(query: object): Promise<any> {
    return await this.repository.findOne(query);
  }

  async update(id, updateDto): Promise<any> {
    await this.repository.update(id, updateDto);
    return await this.findOne({ id });
  }

  async delete(id): Promise<boolean> {
    await this.repository.delete(id);
    return true;
  }

  async checkEntity(id: number) {
    const entity = await this.findOne({ id });
    if (!entity) {
      throw new NotFoundException();
    }
    return entity;
  }
}
