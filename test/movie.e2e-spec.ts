import { AppModule } from '../src/app/app.module';
import { Test, TestingModule } from '@nestjs/testing';
import * as chai from 'chai';
import * as shortid from 'shortid';
import * as request from 'supertest';
import { MovieDto } from '../src/app/modules/movie/movie.dto';
import { Movie } from '../src/app/modules/movie/movie.entity';
import { moduleConfig } from '../src/app/modules/movie/movie.config';
import { CredentialsHelper } from './helpers/credentials.e2e.helper';

describe('[API] Movie Endpoints (e2e)', () => {
  let app;
  const should: any = chai.should();
  let route: string;
  let movieCreated: Movie;
  let credentials: any;
  const genres = ['terror', 'drama', 'comedy', 'action'];
  const genre = genres[Math.floor(Math.random() * genres.length)];

  const movieData: MovieDto = {
    title: shortid.generate() + '_TEST_e2e_MOVIE',
    description: shortid.generate(),
    genre: genre,
    picture: `https://${shortid.generate()}${shortid.generate()}${shortid.generate()}/image/${shortid.generate()}.jpeg`,
  };

  const genreUpdate = genres[Math.floor(Math.random() * genres.length)];

  const movieUpdateData: MovieDto = {
    title: shortid.generate() + '_UPDATED',
    description: shortid.generate() + '_UPDATED',
    genre: genreUpdate,
    picture: `https://${shortid.generate()}${shortid.generate()}${shortid.generate()}/image/${shortid.generate()}_UPDATED.jpeg`,
  };

  beforeAll(async () => {
    route = `/${moduleConfig.api.route}`;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    credentials = await CredentialsHelper.generate();
  }, 10000);

  afterAll(async () => {
    if (app) {
      await app.close();
    }
  });

  it('Create a new movie', done => {
    return request(app.getHttpServer())
      .post(route)
      .send(movieData)
      .set('Authorization', `Bearer ${credentials.access_token}`)
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        movieCreated = res.body;
        res.body.should.have.property('id', movieCreated.id);
        res.body.should.have.property('title', movieData.title);
        res.body.should.have.property('description', movieData.description);
        res.body.should.have.property('genre', movieData.genre);
        res.body.should.have.property('picture', movieData.picture);
        return done();
      });
  });

  it('Get a wrong movie', done => {
    return request(app.getHttpServer())
      .get(`${route}${shortid.generate()}`)
      .expect(404)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('statusCode', 404);
        res.body.should.have.property('error', 'Not Found');
        return done();
      });
  });

  it('Get a movie', done => {
    return request(app.getHttpServer())
      .get(`${route}/${movieCreated.id}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('id', movieCreated.id);
        res.body.should.have.property('title', movieCreated.title);
        res.body.should.have.property('description', movieCreated.description);
        res.body.should.have.property('genre', movieCreated.genre);
        res.body.should.have.property('picture', movieCreated.picture);
        return done();
      });
  });

  it('Get all movie', done => {
    return request(app.getHttpServer())
      .get(route)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.be.instanceof(Array);
        res.body.should.have.length.above(1);
        return done();
      });
  });

  it('Update an movie', done => {
    return request(app.getHttpServer())
      .put(`${route}/${movieCreated.id}`)
      .send(movieUpdateData)
      .set('Authorization', `Bearer ${credentials.access_token}`)
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('title', movieUpdateData.title);
        res.body.should.have.property(
          'description',
          movieUpdateData.description,
        );
        res.body.should.have.property('genre', movieUpdateData.genre);
        res.body.should.have.property('picture', movieUpdateData.picture);
        return done();
      });
  });

  it('Delete an movie', done => {
    return request(app.getHttpServer())
      .delete(`${route}/${movieCreated.id}`)
      .set('Authorization', `Bearer ${credentials.access_token}`)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it('Get a removed movie', done => {
    return request(app.getHttpServer())
      .get(`${route}/${shortid.generate()}`)
      .set('Authorization', `Bearer ${credentials.access_token}`)
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('statusCode', 404);
        res.body.should.have.property('error', 'Not Found');
        return done();
      });
  });
});
