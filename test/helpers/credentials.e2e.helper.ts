import { AuthDto } from '../../src/app/modules/auth/auth.dto';
import { UserDto } from '../../src/app/modules/user/user.dto';
import { User } from '../../src/app/modules/user/user.entity';
import * as debug from 'debug';
import * as shortid from 'shortid';
import * as request from 'supertest';
import { moduleConfig as userModuleConfig } from '../../src/app/modules/user/user.config';
import { moduleConfig as authModuleConfig } from '../../src/app/modules/auth/auth.config';
import { config } from '../../src/config/config';

const log: debug.IDebugger = debug('kubide:credentials:e2e-spec');

export class CredentialsHelper {
  private static userCreated: User;

  public static async generate(): Promise<any> {
    try {
      const api = request(`${config.sites.api.url}:${config.sites.api.port}`);
      let response: any;

      const userData: UserDto = {
        username: shortid.generate() + '_HELPER_e2e_USER_CREDENTIALS',
        password: shortid.generate() + shortid.generate(),
      };

      // Create user
      response = await api
        .post(`/${userModuleConfig.api.route}`)
        .send(userData)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect('Content-Type', /json/)
        .expect(201);
      this.userCreated = response.body;

      const authData: AuthDto = {
        username: this.userCreated.username,
        password: userData.password,
      };

      // Create token
      response = await api
        .post(`/${authModuleConfig.api.route}`)
        .send(authData)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect('Content-Type', /json/)
        .expect(201);
      return response.body;
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
