import { UserDto } from '../../src/app/modules/user/user.dto';
import * as shortid from 'shortid';
import * as request from 'supertest';
import { moduleConfig as userModuleConfig } from '../../src/app/modules/user/user.config';
import { config } from '../../src/config/config';

export class UserHelper {
  private static response: any;

  public static async generate(): Promise<any> {
    try {
      const api = request(`${config.sites.api.url}:${config.sites.api.port}`);

      const userData: UserDto = {
        username: shortid.generate() + '_HELPER_e2e_USER',
        password: shortid.generate() + shortid.generate(),
      };

      // Create user
      this.response = await api
        .post(`/${userModuleConfig.api.route}`)
        .send(userData)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect('Content-Type', /json/)
        .expect(201);

      return userData;
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
