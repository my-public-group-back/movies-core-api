import { AppModule } from '../src/app/app.module';
import { Test, TestingModule } from '@nestjs/testing';
import * as chai from 'chai';
import * as request from 'supertest';
import { User } from '../src/app/modules/user/user.entity';
import { moduleConfig } from '../src/app/modules/auth/auth.config';
import { UserHelper } from './helpers/user.e2e.helper';
import { AuthDto } from '../src/app/modules/auth/auth.dto';

describe('[API] Auth Endpoints (e2e)', () => {
  let app;
  const should: any = chai.should();
  let route: string;
  let userHelper: User;
  let authData: AuthDto;

  beforeAll(async () => {
    route = `/${moduleConfig.api.route}`;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    userHelper = await UserHelper.generate();

    authData = {
      username: userHelper.username,
      password: userHelper.password,
    };
  }, 10000);

  afterAll(async () => {
    if (app) {
      await app.close();
    }
  });

  it('Create a new auth', done => {
    return request(app.getHttpServer())
      .post(route)
      .send(authData)
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('expiresIn');
        res.body.should.have.property('access_token');
        res.body.should.have.property('user');
        return done();
      });
  });
});
