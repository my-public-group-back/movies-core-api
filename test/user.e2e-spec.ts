import { AppModule } from '../src/app/app.module';
import { Test, TestingModule } from '@nestjs/testing';
import * as chai from 'chai';
import * as shortid from 'shortid';
import * as request from 'supertest';
import { User } from '../src/app/modules/user/user.entity';
import { UserDto } from '../src/app/modules/user/user.dto';
import { moduleConfig } from '../src/app/modules/user/user.config';

describe('[API] User Endpoints (e2e)', () => {
  let app;
  const should: any = chai.should();
  let route: string;
  let userCreated: User;

  const userData: UserDto = {
    username: shortid.generate() + '_TEST_e2e_USER',
    password: shortid.generate() + shortid.generate(),
  };

  beforeAll(async () => {
    route = `/${moduleConfig.api.route}`;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  }, 10000);

  afterAll(async () => {
    if (app) {
      await app.close();
    }
  });

  it('Create a new user', done => {
    return request(app.getHttpServer())
      .post(route)
      .send(userData)
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        userCreated = res.body;
        res.body.should.have.property('id', userCreated.id);
        res.body.should.have.property('username', userData.username);
        return done();
      });
  });
});
