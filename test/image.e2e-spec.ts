import { Test, TestingModule } from '@nestjs/testing';
import * as chai from 'chai';
import * as request from 'supertest';
import * as shortid from 'shortid';
import { moduleConfig } from '../src/app/modules/image/image.config';
import { AppModule } from '../src/app/app.module';
import * as fse from 'fs-extra';
import { CredentialsHelper } from './helpers/credentials.e2e.helper';

describe('[API] Image Endpoints (e2e)', () => {
  const should: any = chai.should();
  let app;
  let route: string;
  let imageUploaded;
  let credentials: any;

  beforeAll(async () => {
    route = `/${moduleConfig.api.route}/`;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    credentials = await CredentialsHelper.generate();
  }, 10000);

  afterAll(async () => {
    if (app) {
      await app.close();
    }
  });

  it('Create a new Image without authorization', done => {
    return request(app.getHttpServer())
      .post(route)
      .attach('image', `${__dirname}/attachments/logo-avengers.png`)
      .expect(401)
      .end(async (err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('statusCode', 401);
        return done();
      });
  });

  it('Create a new Image with authorization', done => {
    return request(app.getHttpServer())
      .post(route)
      .set('Authorization', `Bearer ${credentials.access_token}`)
      .attach('image', `${__dirname}/attachments/logo-avengers.png`)
      .expect(201)
      .end(async (err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('filename');
        res.body.should.have.property('path');
        res.body.should.have.property('mimetype', 'image/png');
        imageUploaded = res.body;
        return done();
      });
  });

  it('Create a new Image with invalid profile', done => {
    return request(app.getHttpServer())
      .post(route)
      .set('Authorization', `Bearer ${credentials.access_token}`)
      .attach(shortid.generate(), `${__dirname}/attachments/logo-avengers.png`)
      .expect(400)
      .end(async (err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('statusCode', 400);
        return done();
      });
  });

  it('Create a new Image with invalid mimetype', done => {
    return request(app.getHttpServer())
      .post(route)
      .set('Authorization', `Bearer ${credentials.access_token}`)
      .attach('image', `${__dirname}/attachments/logo-avengers-copia.pdf`)
      .expect(403)
      .end(async (err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('statusCode', 403);
        return done();
      });
  });

  afterAll(async () => {
    await fse.remove(
      `${__dirname}../artifacts/public/images/${imageUploaded.filename}`,
    );
  });
});
