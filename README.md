## Description

NestJs Movies Core Api

[Movies API-Core](https://gitlab.com/my-public-group-back/movies-core-api)

## Architecture
```bash
movies-core-api
  ¬ artifacts
    ¬¬ db
    ¬¬ dist
    ¬¬ public
       ¬¬¬ images
  ¬ services
    ¬¬ docker
       docker-compose.yml
       docker-compose-mysql-db.yml
       pro.Dockerfile
  ¬ src
    ¬¬ app
       ¬¬¬ modules
           ¬¬¬ auth
               auth.config.ts
               auth.controller.ts
               auth.dto.ts
               auth.module.ts
               auth.service.ts
               jwt.strategy.ts
           ¬¬¬ image
               image.config.ts
               image.controller.ts
               image.module.ts
           ¬¬¬ movie
               movie.config.ts
               movie.controller.ts
               movie.dto.ts
               movie.entity.ts
               movie.module.ts
               movie.service.ts
           ¬¬¬ user
               user.config.ts
               user.controller.ts
               user.dto.ts
               user.entity.ts
               user.module.ts
               user.service.ts
       app.controller.spec.ts
       app.controller.ts
       app.module.ts
       app.service.ts
    ¬¬ config
       config.base.ts
       config.project.ts
       config.ts
    ¬¬ shared
       ¬¬¬ services
          mysql.abstract.service.ts
    maint.ts
  ¬ test
    ¬¬ attachments
       logo-avengers.png
       logo-avengers-copia.pdf
    ¬¬ helpers
       credentials.e2e.helper.ts
       user.e2e.helper.ts
    app.e2e-spec.ts
    auth.e2e-spec.ts
    image.e2e-spec.ts
    jest-e2e.ts
    movie.e2e-spec.ts
    user.e2e-spec.ts
  .dockerignore
  .eslintrc.js
  .gitignore
  .gitlab-ci.yml
  .prettier
  nest-cli.json
  nodemon.json
  package.json
  package-lock.json
  README.md
  README.md
  tsconfig.build.json
  tsconfig.json
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# Docker Compose
$ npm run docker:start <- Launchs CoreAPI and DB

# Development
$ npm run docker:mysql <- Launchs DB
$ npm run start:dev <- Launchs Core-API

# Deploy to Production
$ gitlab-ci.yml describes pipeline that must be triggered when code is pushed to master branch:
- There are two stages:
    - Stage One: install_dependencies_and_build
        - npm install
        - npm run build
    - Stage Two: deploy_production -> Heroku
        - https://movies-core-api.herokuapp.com
```
## Use app

```bash
# Endpoints
-  Register User: POST https://movies-core-api.herokuapp.com/users
   Body: {
         	"username": "prueba",
         	"password": "passPrueba"         	
         }

-  Get Token of User: POST https://movies-core-api.herokuapp.com/auths
   Body: {
         	"username": "prueba",
         	"password": "passPrueba"         	
         }

-  Upload picture: POST https://movies-core-api.herokuapp.com/images
   Authorization: 'Bearer asdfghjklmnopqrstuvwxyz'
   Body(multipart-form-data) : image-test.png

-  Get picture: GET https://movies-core-api.herokuapp.com/images/image-test.png

-  Register Movie: POST https://movies-core-api.herokuapp.com/movies
   Authorization: 'Bearer asdfghjklmnopqrstuvwxyz'
   Body: {
            "title": "Batman: The dark knight rises",
            "description": "Peli de murciélagos",
            "genre": "action",
            "picture": "http:localhost:3001/images/777364a38361a2023310f1653241b622d.png"        	
         }

-  Get All Movies: GET https://movies-core-api.herokuapp.com/movies

-  Get One Movie: GET https://movies-core-api.herokuapp.com/movies/:id

-  Update One Movie: PUT https://movies-core-api.herokuapp.com/movies/:id
   Authorization: 'Bearer asdfghjklmnopqrstuvwxyz'
   Body: {
           "title": "Batman: The dark knight rises",
            "description": "Peli de murciélagos",
            "genre": "action",
            "picture": "http:localhost:3001/images/777364a38361a2023310f1653241b622d.png"        	
         }

-  Delete One Movie: DELETE https://movies-core-api.herokuapp.com/movies/:id
   Authorization: 'Bearer asdfghjklmnopqrstuvwxyz'
```

## Test

```bash
# e2e tests
$ npm run test:e2e
```

## Answer
```
Si la aplicación esta teniendo mucho tráfico y estamos teniendo
problemas de rendimiento a la hora de devolver las películas, 
¿Cómo lo solucionarías?
```

## Solution
```
Existen diversas soluciones para el problema de alto tráfico en nuestra aplicación, pero en nuestro caso, 
al haber implementado la base de datos con una base de datos relacional com MySql, se proponen las siguientes medidas:
    - Utilización de índices para hacer las queries más rápidas.
    - Replicación del Core-API en distintas intancias (a nivel geográfico) y direccionamiento de las peticiones de los 
      clientes por proximidad geográfica para aliviar el tráfico.
    - Replicación de la base de datos en diversas instancias para poder direccionar las peticiones a base de datos
    - Utilizar cacheo de datos para minimizar la carga de trabajo de la base de datos.
```

## Stay in touch

- Author - [Jose Lorenzo Moreno Moreno](https://www.linkedin.com/in/jolorenzom/)

## License

  This project is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).
